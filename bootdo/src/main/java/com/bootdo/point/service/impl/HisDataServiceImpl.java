package com.bootdo.point.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.point.dao.HisDataDao;
import com.bootdo.point.domain.HisDataDO;
import com.bootdo.point.service.HisDataService;



@Service
public class HisDataServiceImpl implements HisDataService {
	@Autowired
	private HisDataDao hisDataDao;
	
	@Override
	public HisDataDO get(Integer id){
		return hisDataDao.get(id);
	}
	
	@Override
	public List<HisDataDO> list(Map<String, Object> map){
		return hisDataDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return hisDataDao.count(map);
	}
	
	@Override
	public int save(HisDataDO hisData){
		return hisDataDao.save(hisData);
	}
	
	@Override
	public int update(HisDataDO hisData){
		return hisDataDao.update(hisData);
	}
	
	@Override
	public int remove(Integer id){
		return hisDataDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return hisDataDao.batchRemove(ids);
	}
	
}
