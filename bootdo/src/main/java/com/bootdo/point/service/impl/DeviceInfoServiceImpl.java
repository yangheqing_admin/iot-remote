package com.bootdo.point.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.point.dao.DeviceInfoDao;
import com.bootdo.point.domain.DeviceInfoDO;
import com.bootdo.point.service.DeviceInfoService;



@Service
public class DeviceInfoServiceImpl implements DeviceInfoService {
	@Autowired
	private DeviceInfoDao deviceInfoDao;
	
	@Override
	public DeviceInfoDO get(String deviceId){
		return deviceInfoDao.get(deviceId);
	}
	
	@Override
	public List<DeviceInfoDO> list(Map<String, Object> map){
		return deviceInfoDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return deviceInfoDao.count(map);
	}
	
	@Override
	public int save(DeviceInfoDO deviceInfo){
		return deviceInfoDao.save(deviceInfo);
	}
	
	@Override
	public int update(DeviceInfoDO deviceInfo){
		return deviceInfoDao.update(deviceInfo);
	}
	
	@Override
	public int remove(String deviceId){
		return deviceInfoDao.remove(deviceId);
	}
	
	@Override
	public int batchRemove(String[] deviceIds){
		return deviceInfoDao.batchRemove(deviceIds);
	}
	
}
